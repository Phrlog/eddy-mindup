## развернуть DEV

* поднять конт с БД
```bash
docker-compose up -d
``` 

* наполнить БД
```bash

bin/console app:create-default-test
bin/console app:create-default-specialization
bin/console app:vacansy-load            ## загрузка подробной инфо из API HH
bin/console app:vacansy-add-detail      ## разбираем вакансии на детали
bin/console app:skill-prepare           ## ключевые навыки и их процент вхождения в Вакансии

```




