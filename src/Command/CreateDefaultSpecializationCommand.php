<?php

namespace App\Command;

use App\Entity\Specialization;
use App\Entity\Test;
use App\Entity\TestQuestion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateDefaultSpecializationCommand extends Command
{
    private const DEFAULT_SPECILIZATION = [
        'PHP программист',
        'JS программист',
        'Data scientist',
        'Java программист',
        'Программист Python',
        'Frontend разработчик',
        'Backend программист',
        'UX дизайнер',
        'Веб дизайнер',
        'Дизайнер интерфейсов',
        'Дизайнер мобильных приложений',
    ];

    protected static $defaultName = 'app:create-default-specialization';
    /**
     * @var EntityManagerInterface
     */
    private $em;

    protected function configure()
    {
        $this
            ->setDescription('Добавление в таблицу специальностей по умолчанию.')
        ;
    }

    public function __construct(string $name = null, EntityManagerInterface $em)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        foreach (self::DEFAULT_SPECILIZATION as $spec) {
            $newSpecialization = new Specialization();
            $newSpecialization->setName($spec);
            $this->em->persist($newSpecialization);
        }

        $this->em->flush();

        $io->success('Added default specialization SUCCESS.');

        return 0;
    }
}
