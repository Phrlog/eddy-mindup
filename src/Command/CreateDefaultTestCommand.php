<?php

namespace App\Command;

use App\Entity\Test;
use App\Entity\TestQuestion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateDefaultTestCommand extends Command
{
    private const DEFAULT_TEST = [
        ['Да', 'Нет', '1.Я люблю сочинять собственные песни.'],
        ['Да', 'Нет', '2.Я люблю гулять один.'],
        ['Да', 'Нет', '3.Мои папа и мама любят играть со мной.'],
        ['Да', 'Нет', '4.Я задаю много вопросов.'],
        ['Да', 'Нет', '5.Сочинение рассказов и сказок – пустое занятие.'],
        ['Да', 'Нет', '6.Я люблю, чтобы у меня был только один или два друга.'],
        ['Да', 'Нет', '7.Я ничего не имею против, если иногда меняются правила игры.'],
        ['Да', 'Нет', '8.У меня есть действительно несколько хороших идей.'],
        ['Да', 'Нет', '9.Я люблю рисовать.'],
        ['Да', 'Нет', '10.Я люблю вещи, которые трудно делать.'],
        ['Да', 'Нет', '11.Солнце на рисунке должно быть всегда желтым.'],
        ['Да', 'Нет', '12.Я люблю все разбирать, чтобы понять, как это работает.'],
        ['Да', 'Нет', '13.Мне больше нравится раскрашивать картинки, чем рисовать самому.'],
        ['Да', 'Нет', '14.Легкие задачки – самые интересные.'],
        ['Да', 'Нет', '15.Иногда папа или мама занимаются чем-нибудь вместе со мной.'],
        ['Да', 'Нет', '16.Я люблю узнавать новое о животных.'],
        ['Да', 'Нет', '17.Мой папа любит делать что-нибудь по дому.'],
        ['Да', 'Нет', '18.Я не люблю, когда другие дети задают много вопросов.'],
        ['Да', 'Нет', '19.Трудно найти себе занятие, когда находишься один.'],
        ['Да', 'Нет', '20.Мой папа думает, что я обычно поступаю правильно.'],
        ['Да', 'Нет', '21.Я люблю рассказы о далеком прошлом.'],
        ['Да', 'Нет', '22.Я охотнее играю в старые игры, чем в новые.'],
        ['Да', 'Нет', '23.Когда я хочу что-то сделать, но мне это трудно, я отказываюсь от этой затеи и берусь за что-нибудь другое.'],
        ['Да', 'Нет', '24.Я всегда играю с друзьями, а один я не люблю играть.'],
    ];
    protected static $defaultName = 'app:create-default-test';
    /**
     * @var EntityManagerInterface
     */
    private $em;

    protected function configure()
    {
        $this
            ->setDescription('Добавление Теста по умолчанию в таблицу БД.')
        ;
    }

    public function __construct(string $name = null, EntityManagerInterface $em)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $defaultTest = new Test();
        $defaultTest->setName('Определение твоих творческих расположенностей.')
            ->setDescription('Рекомендованный способ для выявления творческого начала и предрасположенности.')
            ->setSection('main')
        ;
        $this->em->persist($defaultTest);

        foreach (self::DEFAULT_TEST as $question) {
            $testQuestion = new TestQuestion();
            $testQuestion->setTest($defaultTest)
                ->setText($question[2])
                ->setAnswerOptions(['yes' => $question[0], 'no' => $question[1]])
            ;

            $this->em->persist($testQuestion);
        }

        $this->em->flush();

        $io->success('Added default test SUCCESS.');

        return 0;
    }
}
