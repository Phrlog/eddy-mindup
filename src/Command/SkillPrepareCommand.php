<?php

namespace App\Command;

use App\Entity\SkillDetail;
use App\Repository\SpecializationRepository;
use App\Repository\VacansyDetailRepository;
use App\Repository\VacansyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SkillPrepareCommand extends Command
{
    protected static $defaultName = 'app:skill-prepare';
    /**
     * @var VacansyRepository
     */
    private $vacansyRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var VacansyDetailRepository
     */
    private $vacansyDetailRepository;
    /**
     * @var SpecializationRepository
     */
    private $specializationRepository;

    public function __construct(
        string $name = null,
        VacansyRepository $vacansyRepository,
        EntityManagerInterface  $entityManager,
        VacansyDetailRepository $vacansyDetailRepository,
        SpecializationRepository $specializationRepository
    )
    {
        parent::__construct($name);
        $this->vacansyRepository = $vacansyRepository;
        $this->entityManager = $entityManager;
        $this->vacansyDetailRepository = $vacansyDetailRepository;
        $this->specializationRepository = $specializationRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Получаем данные об вакансиях и скилах и процентах')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $specializations = $this->specializationRepository->findAll();

        foreach ($specializations as $specialization) {
            $allVacansy = $specialization->getVacansyDetails();
            $countVacansy = count($allVacansy);

            $tmpKeySkills = [];
            $result= [];

            foreach ($allVacansy as $vacansy) {
                $keySkills = json_decode($vacansy->getKeySkill(), true);

                foreach ($keySkills as $skill) {
                    if (!in_array($skill, $tmpKeySkills))
                        $tmpKeySkills[] = $skill;
                }
            }

            foreach ($tmpKeySkills as $oneSkill) {
                $result[$oneSkill] = 0;
                foreach ($allVacansy as $vacansy) {
                    if (in_array($oneSkill, json_decode($vacansy->getKeySkill(), true))) {
                        $result[$oneSkill] += 1;
                    }
                }
            }

            foreach ($result as $skill => &$count) {
                $count = (int) ceil($count/$countVacansy * 100) + 17;
            }

            array_multisort($result, SORT_DESC);

            $data = array_slice($result, 0, 35);


            foreach ($data as $skillName => $percent) {
                $skillDetail = new SkillDetail();
                $skillDetail
                    ->setSpecialization($specialization)
                    ->setTitle($skillName)
                    ->setPercent($percent)
                ;

                $this->entityManager->persist($skillDetail);
            }

            $this->entityManager->flush();
        }


        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
    }
}
