<?php

namespace App\Command;

use App\Entity\VacansyDetail;
use App\Repository\VacansyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class VacansyAddDetailCommand extends Command
{
    /**
     * @var VacansyRepository
     */
    private $vacansyRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        string $name = null,
        VacansyRepository $vacansyRepository,
        EntityManagerInterface  $entityManager
    ) {
        parent::__construct($name);
        $this->vacansyRepository = $vacansyRepository;
        $this->entityManager = $entityManager;
    }

    protected static $defaultName = 'app:vacansy-add-detail';

    protected function configure()
    {
        $this
            ->setDescription('Разбираем вакансии и сохраняем из них нужную инфу')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        // Выбираем по 300 необработанных вакансий
        while ($vacansies = $this->vacansyRepository->findBy(['isProcesssed' => false], null, 300)) {
            foreach ($vacansies as $vacansy) {
                $vacansyData = json_decode($vacansy->getRowData(), true);

                // костыль, если вдруг битый JSON
                if (null == $vacansyData) {
                    $vacansy = $vacansy->setIsProcesssed(true);
                    $this->entityManager->persist($vacansy);
                    $this->entityManager->flush();

                    cotinue;
                }


                $salaryCurrency = !empty($vacansyData['salary']['currency']) ? $vacansyData['salary']['currency'] : '';
                $employerId = !empty($vacansyData['employer']['id']) ? $vacansyData['employer']['id'] : 0;

                $vacansyDetail = new VacansyDetail();
                $vacansyDetail
                    ->setSpecialization($vacansy->getSpecialization())
                    ->setHhId($vacansyData['id'])
                    ->setName($vacansyData['name'])
                    ->setAreaId($vacansyData['area']['id'])
                    ->setAreaName($vacansyData['area']['name'])
                    ->setSalaryFrom($vacansyData['salary']['from'])
                    ->setSalaryTo($vacansyData['salary']['to'])
                    ->setSalaryCurrency($salaryCurrency)
                    ->setExperienceId($vacansyData['experience']['id'])
                    ->setExperienceName($vacansyData['experience']['name'])
                    ->setDescription($vacansyData['description'])
                    ->setEmployerId($employerId)
                    ->setEmployerName($vacansyData['employer']['name'])
                    ->setKeySkill($this->getKeySkills($vacansyData))
                ;

                $vacansy = $vacansy->setIsProcesssed(true);

                $this->entityManager->persist($vacansy);
                $this->entityManager->persist($vacansyDetail);
            }

            $this->entityManager->flush();


            var_dump(__LINE__);
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
    }

    /**
     * @param array $vacansyData
     * @return string
     */
    private function getKeySkills(array $vacansyData): string
    {
        $resultSkills = [];
        foreach ($vacansyData['key_skills'] as $skill) {
            $resultSkills[] = $skill['name'];
        }

        return json_encode($resultSkills, JSON_UNESCAPED_UNICODE);
    }
}
