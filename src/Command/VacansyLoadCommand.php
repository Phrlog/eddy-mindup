<?php

namespace App\Command;

use App\Entity\Vacansy;
use App\Repository\SpecializationRepository;
use App\Vacansy\VacansyProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use seregazhuk\HeadHunterApi\Api;
use seregazhuk\HeadHunterApi\EndPoints\EndpointsContainer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class VacansyLoadCommand extends Command
{
    private const ID_CITY_MOSCOW = 1;

    protected static $defaultName = 'app:vacansy-load';
    /**
     * @var VacansyProviderInterface
     */
    private $transport;
    /**
     * @var SpecializationRepository
     */
    private $specializationRepository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var string
     */
    private $name;
    /**
     * @var EndpointsContainer
     */
    private $clientHh;

    public function __construct(
        string $name = null,
        SpecializationRepository $specializationRepository,
        VacansyProviderInterface $transport,
        EntityManagerInterface $em,
        EndpointsContainer $clientHh
    ) {
        parent::__construct($name);
        $this->transport = $transport;
        $this->specializationRepository = $specializationRepository;
        $this->em = $em;
        $this->name = $name;
        $this->clientHh = $clientHh;
    }

    protected function configure()
    {
        $this
            ->setDescription('Load vacansy by key. Source hh.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $specializations = $this->specializationRepository->findAll();

        foreach ($specializations as $specialization) {
            $vacancies = $this->transport->getVacancies($specialization->getName(), self::ID_CITY_MOSCOW);

            // здесь взять всю вакансию и сохранить в таблицу
            foreach ($vacancies as $vacancyFromHh) {

                // получаем полные сведения о вакансии
                $vacansyRow = $this->clientHh->vacancies->view($vacancyFromHh['id']);

                $length = mb_strlen(json_encode($vacansyRow, JSON_UNESCAPED_UNICODE));
                if ($length > 20000) {
                    echo 'БОЛЕЕ 20.000 тысяч';
                    continue;
                }

                $vacancy = new Vacansy();
                $vacancy->setHhId($vacancyFromHh['id'])
                    ->setSpecialization($specialization)
                    ->setRowData(json_encode($vacansyRow, JSON_UNESCAPED_UNICODE))
                ;
                $this->em->persist($vacancy);
            }
            $this->em->flush();
            var_dump($specialization->getName(), count($vacancies));
//            die();
        }


        $io->success('Загружены вакансии для специальностей');

        return 0;
    }



//
//        string(26) "PHP программист"
//        int(1252)
//        string(25) "JS программист"
//        int(2000)
//        string(14) "Data scientist"
//        int(239)
//        string(27) "Java программист"
//        int(2000)
//        string(29) "Программист Python"
//        int(1551)
//        string(31) "Frontend разработчик"
//        int(1500)
//        string(30) "Backend программист"
//        int(1479)
//        string(19) "UX дизайнер"
//        int(477)
//        string(23) "Веб дизайнер"
//        int(1040)
//        string(39) "Дизайнер интерфейсов"
//        int(169)
//        string(56) "Дизайнер мобильных приложений"
//        int(428)


}
