<?php

namespace App\Controller;

use App\Repository\SpecializationRepository;
use App\User\Create\Command as CreateUserCommand;
use App\User\SaveQuestion\Command as SaveQuestionCommand;
use Formapro\TelegramBot\Bot;
use Formapro\TelegramBot\InlineKeyboardButton;
use Formapro\TelegramBot\InlineKeyboardMarkup;
use Formapro\TelegramBot\ReplyKeyboardRemove;
use Symfony\Component\HttpFoundation\Response;
use Formapro\TelegramBot\Update;
use Formapro\TelegramBot\SendMessage;
use Formapro\TelegramBot\KeyboardButton;
use Formapro\TelegramBot\ReplyKeyboardMarkup;
use App\User\SaveQuestion\Handler as SaveQuestion;
use App\User\Create\Handler as CreateUser;

class BotController
{
    private $bot;

    private $createUser;

    private $saveQuestion;

    private $specializationRepository;

    public function __construct(
        Bot $bot,
        CreateUser $createUser,
        SaveQuestion $saveQuestion,
        SpecializationRepository $specializationRepository
    ) {
        $this->bot = $bot;
        $this->createUser = $createUser;
        $this->saveQuestion = $saveQuestion;
        $this->specializationRepository = $specializationRepository;
    }

    public function handle()
    {
        $requestBody = file_get_contents('php://input');
        $data = json_decode($requestBody, true);

        $update = Update::create($data);

        $defaultMarkup = new ReplyKeyboardMarkup([
            [new KeyboardButton('/test')],
            [new KeyboardButton('/about')],
            [new KeyboardButton('/skills')],
        ]);

        $allowedAnswersForTest1 = ['Сериалы', 'Кино', 'Видеоролики на Youtube', 'Стриминг'];

        if ($update->getMessage()->getText() === '/test' || in_array($update->getMessage()->getText(), $allowedAnswersForTest1, true)) {

            $command = CreateUserCommand::createFromUser($update->getMessage()->getFrom());

            $this->createUser->execute($command);

            $keyboard = new ReplyKeyboardMarkup([
                    [new KeyboardButton('Сериалы')],
                    [new KeyboardButton('Кино')],
                    [new KeyboardButton('Видеоролики на Youtube')],
                    [new KeyboardButton('Стриминг')],
                    [new KeyboardButton('Закончил с видео')],
            ]);

            $sendMessage = new SendMessage($update->getMessage()->getChat()->getId(), 'Видео:');
            $sendMessage->setReplyMarkup($keyboard);

            $this->bot->sendMessage($sendMessage);

            return new Response('', 200);
        }

        if ($update->getMessage()->getText() === 'Закончил с видео') {
            // TODO сохраняем ответ на первый вопрос

            $this->saveQuestion->execute(
                new SaveQuestionCommand(
                    $update->getMessage()->getFrom()->getUsername(),
                    'Видео',
                    $update->getMessage()->getText()
                )
            );

            /**
             * Захардкодим здесь $questionRepository->getByName('question2')->getAnswers
             * из ответов создаем new KeyboardButton($answer)
             */

            $keyboard = new ReplyKeyboardMarkup([
                [new KeyboardButton('Слушаю часто')],
                [new KeyboardButton('Слушаю редко')],
                [new KeyboardButton('Учу тексты наизусть')],
            ]);

            $sendMessage = new SendMessage($update->getMessage()->getChat()->getId(), 'Музыка:');
            $sendMessage->setReplyMarkup($keyboard);

            $this->bot->sendMessage($sendMessage);

            return new Response('', 200);
        }

        $allowedAnswersForTest2 = ['Слушаю часто', 'Слушаю редко', 'Учу тексты наизусть'];
        $allowedAnswersForTest3 = ['Соцсети', 'Рисование', 'Спорт', 'Компьютерные игры', 'Мобильные игры'];

        if (
            in_array($update->getMessage()->getText(), $allowedAnswersForTest2, true)
            ||
            in_array($update->getMessage()->getText(), $allowedAnswersForTest3, true) ) {
            $this->saveQuestion->execute(
                new SaveQuestionCommand(
                    $update->getMessage()->getFrom()->getUsername(),
                    'Музыка',
                    $update->getMessage()->getText()
                )
            );

            $keyboard = new ReplyKeyboardMarkup([
                [new KeyboardButton('Соцсети')],
                [new KeyboardButton('Рисование')],
                [new KeyboardButton('Спорт')],
                [new KeyboardButton('Компьютерные игры')],
                [new KeyboardButton('Мобильные игры')],
                [new KeyboardButton('Закончил с хобби')],
            ]);

            $sendMessage = new SendMessage($update->getMessage()->getChat()->getId(), 'Хобби:');
            $sendMessage->setReplyMarkup($keyboard);

            $this->bot->sendMessage($sendMessage);

            return new Response('', 200);
        }

        if ($update->getMessage()->getText() === 'Закончил с хобби') {
            $this->saveQuestion->execute(
                new SaveQuestionCommand(
                    $update->getMessage()->getFrom()->getUsername(),
                    'Хобби',
                    $update->getMessage()->getText()
                )
            );

            $sendMessage = new SendMessage($update->getMessage()->getChat()->getId(), 'Спасибо за ответы!');

            $button = InlineKeyboardButton::withUrl(
                'Заходи к нам в гости. Твои школьные друзья уже ждут!',
                'http://phrlog.tk/index.html?user=' . md5(time())
            );

            $keyboard = new InlineKeyboardMarkup([[$button]]);

            $sendMessage->setReplyMarkup($keyboard);

            $this->bot->sendMessage($sendMessage);

            return new Response('', 200);
        }

        if ($update->getMessage()->getText() === '/about') {
            $sendMessage = new SendMessage($update->getMessage()->getChat()->getId(), 'Stunningly simple and Amazingly effective education');

            $sendMessage->setReplyMarkup($defaultMarkup);

            $this->bot->sendMessage($sendMessage);

            return new Response('', 200);
        }

        $specs = $this->specializationRepository->findAll();

        if ($update->getMessage()->getText() === '/skills') {

            $buttonsArray = [];

            foreach ($specs as $spec) {
               $buttonsArray[] = [new KeyboardButton($spec->getName())];
            }

            $keyboard = new ReplyKeyboardMarkup($buttonsArray);

            $sendMessage = new SendMessage($update->getMessage()->getChat()->getId(), 'Выберите специализацию:');

            $sendMessage->setReplyMarkup($keyboard);

            $this->bot->sendMessage($sendMessage);

            return new Response('', 200);
        }

        foreach ($specs as $spec) {
            if ($update->getMessage()->getText() === $spec->getName()) {
                $details = $spec->getSkillDetails();

                $textMessage = '';

                foreach ($details as $detail) {
                    $textMessage .= $detail->getTitle() . ': ' . $detail->getPercent() . '%' . PHP_EOL;
                }

                $sendMessage = new SendMessage($update->getMessage()->getChat()->getId(), $textMessage);

                $sendMessage->setReplyMarkup($defaultMarkup);

                $this->bot->sendMessage($sendMessage);

                return new Response('', 200);
            }
        }

        $welcomeText = <<<TEXT
Привет! Я — образовательная платформа EDDY от команды MindUP! Team.

/test — Начать тестирование
/about — О проекте
/skills — Необходимые навыки для выбранной специализации
TEXT;

        $sendMessage = new SendMessage(
            $update->getMessage()->getChat()->getId(),
            $welcomeText
        );

        $sendMessage->setReplyMarkup(
            $defaultMarkup
        );

        $this->bot->sendMessage($sendMessage);

        return new Response('', 200);
    }
}
