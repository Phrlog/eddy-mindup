<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SpecializationRepository")
 */
class Specialization
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vacansy", mappedBy="specialization")
     */
    private $vacansies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VacansyDetail", mappedBy="specialization")
     */
    private $vacansyDetails;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SkillDetail", mappedBy="specialization")
     */
    private $skillDetails;

    public function __construct()
    {
        $this->vacansies = new ArrayCollection();
        $this->vacansyDetails = new ArrayCollection();
        $this->skillDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Vacansy[]
     */
    public function getVacansies(): Collection
    {
        return $this->vacansies;
    }

    public function addVacansy(Vacansy $vacansy): self
    {
        if (!$this->vacansies->contains($vacansy)) {
            $this->vacansies[] = $vacansy;
            $vacansy->setSpecialization($this);
        }

        return $this;
    }

    public function removeVacansy(Vacansy $vacansy): self
    {
        if ($this->vacansies->contains($vacansy)) {
            $this->vacansies->removeElement($vacansy);
            // set the owning side to null (unless already changed)
            if ($vacansy->getSpecialization() === $this) {
                $vacansy->setSpecialization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|VacansyDetail[]
     */
    public function getVacansyDetails(): Collection
    {
        return $this->vacansyDetails;
    }

    public function addVacansyDetail(VacansyDetail $vacansyDetail): self
    {
        if (!$this->vacansyDetails->contains($vacansyDetail)) {
            $this->vacansyDetails[] = $vacansyDetail;
            $vacansyDetail->setSpecialization($this);
        }

        return $this;
    }

    public function removeVacansyDetail(VacansyDetail $vacansyDetail): self
    {
        if ($this->vacansyDetails->contains($vacansyDetail)) {
            $this->vacansyDetails->removeElement($vacansyDetail);
            // set the owning side to null (unless already changed)
            if ($vacansyDetail->getSpecialization() === $this) {
                $vacansyDetail->setSpecialization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SkillDetail[]
     */
    public function getSkillDetails(): Collection
    {
        return $this->skillDetails;
    }

    public function addSkillDetail(SkillDetail $skillDetail): self
    {
        if (!$this->skillDetails->contains($skillDetail)) {
            $this->skillDetails[] = $skillDetail;
            $skillDetail->setSpecialization($this);
        }

        return $this;
    }

    public function removeSkillDetail(SkillDetail $skillDetail): self
    {
        if ($this->skillDetails->contains($skillDetail)) {
            $this->skillDetails->removeElement($skillDetail);
            // set the owning side to null (unless already changed)
            if ($skillDetail->getSpecialization() === $this) {
                $skillDetail->setSpecialization(null);
            }
        }

        return $this;
    }
}
