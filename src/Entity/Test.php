<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestRepository")
 */
class Test
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $section;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TestQuestion", mappedBy="test")
     */
    private $testQuestions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TestLog", mappedBy="test")
     */
    private $testLogs;

    public function __construct()
    {
        $this->testQuestions = new ArrayCollection();
        $this->testLogs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(?string $section): self
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @return Collection|TestQuestion[]
     */
    public function getTestQuestions(): Collection
    {
        return $this->testQuestions;
    }

    public function addTestQuestion(TestQuestion $testQuestion): self
    {
        if (!$this->testQuestions->contains($testQuestion)) {
            $this->testQuestions[] = $testQuestion;
            $testQuestion->setTest($this);
        }

        return $this;
    }

    public function removeTestQuestion(TestQuestion $testQuestion): self
    {
        if ($this->testQuestions->contains($testQuestion)) {
            $this->testQuestions->removeElement($testQuestion);
            // set the owning side to null (unless already changed)
            if ($testQuestion->getTest() === $this) {
                $testQuestion->setTest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TestLog[]
     */
    public function getTestLogs(): Collection
    {
        return $this->testLogs;
    }

    public function addTestLog(TestLog $testLog): self
    {
        if (!$this->testLogs->contains($testLog)) {
            $this->testLogs[] = $testLog;
            $testLog->setTest($this);
        }

        return $this;
    }

    public function removeTestLog(TestLog $testLog): self
    {
        if ($this->testLogs->contains($testLog)) {
            $this->testLogs->removeElement($testLog);
            // set the owning side to null (unless already changed)
            if ($testLog->getTest() === $this) {
                $testLog->setTest(null);
            }
        }

        return $this;
    }
}
