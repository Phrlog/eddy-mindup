<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestLogRepository")
 */
class TestLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Test", inversedBy="testLogs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $test;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TestQuestion", inversedBy="testLogs")
     */
    private $testQuestion;

    /**
     * @ORM\Column(type="json")
     */
    private $answer = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    public function setTest(?Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getTestQuestion(): ?TestQuestion
    {
        return $this->testQuestion;
    }

    public function setTestQuestion(?TestQuestion $testQuestion): self
    {
        $this->testQuestion = $testQuestion;

        return $this;
    }

    public function getAnswer(): ?array
    {
        return $this->answer;
    }

    public function setAnswer(array $answer): self
    {
        $this->answer = $answer;

        return $this;
    }
}
