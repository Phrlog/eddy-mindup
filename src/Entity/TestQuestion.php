<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestQuestionRepository")
 */
class TestQuestion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Test", inversedBy="testQuestions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $test;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $text;

    /**
     * @ORM\Column(type="json")
     */
    private $answerOptions = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TestLog", mappedBy="testQuestion")
     */
    private $testLogs;

    public function __construct()
    {
        $this->testLogs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTest(): ?Test
    {
        return $this->test;
    }

    public function setTest(?Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getAnswerOptions(): ?array
    {
        return $this->answerOptions;
    }

    public function setAnswerOptions(array $answerOptions): self
    {
        $this->answerOptions = $answerOptions;

        return $this;
    }

    /**
     * @return Collection|TestLog[]
     */
    public function getTestLogs(): Collection
    {
        return $this->testLogs;
    }

    public function addTestLog(TestLog $testLog): self
    {
        if (!$this->testLogs->contains($testLog)) {
            $this->testLogs[] = $testLog;
            $testLog->setTestQuestion($this);
        }

        return $this;
    }

    public function removeTestLog(TestLog $testLog): self
    {
        if ($this->testLogs->contains($testLog)) {
            $this->testLogs->removeElement($testLog);
            // set the owning side to null (unless already changed)
            if ($testLog->getTestQuestion() === $this) {
                $testLog->setTestQuestion(null);
            }
        }

        return $this;
    }
}
