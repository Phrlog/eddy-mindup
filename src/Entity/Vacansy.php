<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VacansyRepository")
 */
class Vacansy
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Specialization", inversedBy="vacansies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $specialization;

    /**
     * @ORM\Column(type="integer")
     */
    private $hhId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $rowData;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isProcesssed = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecialization(): ?Specialization
    {
        return $this->specialization;
    }

    public function setSpecialization(?Specialization $specialization): self
    {
        $this->specialization = $specialization;

        return $this;
    }

    public function getHhId(): ?int
    {
        return $this->hhId;
    }

    public function setHhId(int $hhId): self
    {
        $this->hhId = $hhId;

        return $this;
    }

    public function getRowData(): ?string
    {
        return $this->rowData;
    }

    public function setRowData(string $rowData): self
    {
        $this->rowData = $rowData;

        return $this;
    }

    public function getIsProcesssed(): ?bool
    {
        return $this->isProcesssed;
    }

    public function setIsProcesssed(bool $isProcesssed): self
    {
        $this->isProcesssed = $isProcesssed;

        return $this;
    }
}
