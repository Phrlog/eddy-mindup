<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VacansyDetailRepository")
 */
class VacansyDetail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Specialization", inversedBy="vacansyDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $specialization;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $areaId;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $areaName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salaryFrom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salaryTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salaryCurrency;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $experienceId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $experienceName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    private $keySkill;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $employerId;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $employerName;

    /**
     * @ORM\Column(type="integer")
     */
    private $hhId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecialization(): ?Specialization
    {
        return $this->specialization;
    }

    public function setSpecialization(?Specialization $specialization): self
    {
        $this->specialization = $specialization;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAreaId(): ?int
    {
        return $this->areaId;
    }

    public function setAreaId(int $areaId): self
    {
        $this->areaId = $areaId;

        return $this;
    }

    public function getAreaName(): ?string
    {
        return $this->areaName;
    }

    public function setAreaName(string $areaName): self
    {
        $this->areaName = $areaName;

        return $this;
    }

    public function getSalaryFrom(): ?string
    {
        return $this->salaryFrom;
    }

    public function setSalaryFrom(?string $salaryFrom): self
    {
        $this->salaryFrom = $salaryFrom;

        return $this;
    }

    public function getSalaryTo(): ?string
    {
        return $this->salaryTo;
    }

    public function setSalaryTo(?string $salaryTo): self
    {
        $this->salaryTo = $salaryTo;

        return $this;
    }

    public function getSalaryCurrency(): ?string
    {
        return $this->salaryCurrency;
    }

    public function setSalaryCurrency(string $salaryCurrency): self
    {
        $this->salaryCurrency = $salaryCurrency;

        return $this;
    }

    public function getExperienceId(): ?string
    {
        return $this->experienceId;
    }

    public function setExperienceId(string $experienceId): self
    {
        $this->experienceId = $experienceId;

        return $this;
    }

    public function getExperienceName(): ?string
    {
        return $this->experienceName;
    }

    public function setExperienceName(?string $experienceName): self
    {
        $this->experienceName = $experienceName;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getKeySkill(): ?string
    {
        return $this->keySkill;
    }

    public function setKeySkill(?string $keySkill): self
    {
        $this->keySkill = $keySkill;

        return $this;
    }

    public function getEmployerId(): ?int
    {
        return $this->employerId;
    }

    public function setEmployerId(?int $employerId): self
    {
        $this->employerId = $employerId;

        return $this;
    }

    public function getEmployerName(): ?string
    {
        return $this->employerName;
    }

    public function setEmployerName(?string $employerName): self
    {
        $this->employerName = $employerName;

        return $this;
    }

    public function getHhId(): ?int
    {
        return $this->hhId;
    }

    public function setHhId(int $hhId): self
    {
        $this->hhId = $hhId;

        return $this;
    }
}
