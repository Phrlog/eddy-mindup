<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191026122217 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vacansy (id INT AUTO_INCREMENT NOT NULL, specialization_id INT NOT NULL, hh_id INT NOT NULL, row_data LONGTEXT NOT NULL, is_processsed TINYINT(1) NOT NULL, INDEX IDX_E3F67EECFA846217 (specialization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE skill_detail (id INT AUTO_INCREMENT NOT NULL, specialization_id INT NOT NULL, title VARCHAR(512) NOT NULL, percent VARCHAR(255) NOT NULL, INDEX IDX_14C2B87FA846217 (specialization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vacansy_detail (id INT AUTO_INCREMENT NOT NULL, specialization_id INT NOT NULL, name VARCHAR(1024) NOT NULL, area_id INT NOT NULL, area_name VARCHAR(1024) NOT NULL, salary_from VARCHAR(255) DEFAULT NULL, salary_to VARCHAR(255) DEFAULT NULL, salary_currency VARCHAR(255) DEFAULT NULL, experience_id VARCHAR(255) DEFAULT NULL, experience_name VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, key_skill VARCHAR(5000) DEFAULT NULL, employer_id INT DEFAULT NULL, employer_name VARCHAR(1024) DEFAULT NULL, hh_id INT NOT NULL, INDEX IDX_21A373E9FA846217 (specialization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE specialization (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(1024) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vacansy ADD CONSTRAINT FK_E3F67EECFA846217 FOREIGN KEY (specialization_id) REFERENCES specialization (id)');
        $this->addSql('ALTER TABLE skill_detail ADD CONSTRAINT FK_14C2B87FA846217 FOREIGN KEY (specialization_id) REFERENCES specialization (id)');
        $this->addSql('ALTER TABLE vacansy_detail ADD CONSTRAINT FK_21A373E9FA846217 FOREIGN KEY (specialization_id) REFERENCES specialization (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE vacansy DROP FOREIGN KEY FK_E3F67EECFA846217');
        $this->addSql('ALTER TABLE skill_detail DROP FOREIGN KEY FK_14C2B87FA846217');
        $this->addSql('ALTER TABLE vacansy_detail DROP FOREIGN KEY FK_21A373E9FA846217');
        $this->addSql('DROP TABLE vacansy');
        $this->addSql('DROP TABLE skill_detail');
        $this->addSql('DROP TABLE vacansy_detail');
        $this->addSql('DROP TABLE specialization');
    }
}
