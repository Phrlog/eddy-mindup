<?php

namespace App\Repository;

use App\Entity\EducationalInstitution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EducationalInstitution|null find($id, $lockMode = null, $lockVersion = null)
 * @method EducationalInstitution|null findOneBy(array $criteria, array $orderBy = null)
 * @method EducationalInstitution[]    findAll()
 * @method EducationalInstitution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EducationalInstitutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EducationalInstitution::class);
    }

    // /**
    //  * @return EducationalInstitution[] Returns an array of EducationalInstitution objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EducationalInstitution
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
