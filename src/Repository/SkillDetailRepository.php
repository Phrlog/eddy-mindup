<?php

namespace App\Repository;

use App\Entity\SkillDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SkillDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method SkillDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method SkillDetail[]    findAll()
 * @method SkillDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkillDetailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SkillDetail::class);
    }

    // /**
    //  * @return SkillDetail[] Returns an array of SkillDetail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SkillDetail
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
