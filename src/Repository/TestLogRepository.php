<?php

namespace App\Repository;

use App\Entity\TestLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TestLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestLog[]    findAll()
 * @method TestLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestLog::class);
    }

    // /**
    //  * @return TestLog[] Returns an array of TestLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TestLog
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
