<?php

namespace App\Repository;

use App\Entity\VacansyDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method VacansyDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method VacansyDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method VacansyDetail[]    findAll()
 * @method VacansyDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VacansyDetailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VacansyDetail::class);
    }

    // /**
    //  * @return VacansyDetail[] Returns an array of VacansyDetail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VacansyDetail
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
