<?php

namespace App\Repository;

use App\Entity\Vacansy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Vacansy|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vacansy|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vacansy[]    findAll()
 * @method Vacansy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VacansyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vacansy::class);
    }

    // /**
    //  * @return Vacansy[] Returns an array of Vacansy objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vacansy
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
