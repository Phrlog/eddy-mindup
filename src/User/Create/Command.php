<?php

namespace App\User\Create;


class Command
{
    public $firstName;
    public $username;

    public static function createFromUser(\Formapro\TelegramBot\User $user): self
    {
        return new self($user->getFirstName(), $user->getUsername());
    }

    private function __construct(string $firstName, string $username)
    {
        $this->firstName = $firstName;
        $this->username = $username;
    }
}
