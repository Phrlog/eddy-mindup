<?php

namespace App\User\Create;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class Handler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $repository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $repository)
    {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    public function execute(Command $request)
    {
        $user = $this->repository->findOneByTelegramName($request->username);

        if (null !== $user) {
            $user->setFirstName($request->firstName);
        } else {
            $user = new User();
            $user->setFirstName($request->firstName)
                 ->setTelegramName($request->username)
            ;
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
