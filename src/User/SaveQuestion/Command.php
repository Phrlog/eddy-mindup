<?php

namespace App\User\SaveQuestion;


class Command
{
    public $username;
    public $question;
    public $answer;

    public function __construct(string $username, string $question, string $answer)
    {
        $this->username = $username;
        $this->question = $question;
        $this->answer = $answer;
    }
}
