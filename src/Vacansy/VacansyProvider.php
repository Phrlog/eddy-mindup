<?php


namespace App\Vacansy;


use seregazhuk\HeadHunterApi\EndPoints\EndpointsContainer;

class VacansyProvider implements VacansyProviderInterface
{
    /**
     * @var EndpointsContainer
     */
    private $clientHh;

    public function __construct(EndpointsContainer $clientHh)
    {
        $this->clientHh = $clientHh;
    }

    public function getVacancies($vacansyName, $idRegion)
    {
        $allVacansies = [];

        for ($idPage = 0; $idPage <= 19; $idPage++) {
            $vacansies = $this->get100vacansies($vacansyName, $idPage, $idRegion);

            // если вакансии закончились, то ранний выход из цикла
            if (null === $vacansies) {
                break;
            }

            $allVacansies = array_merge($allVacansies, $vacansies);
        }

        return $allVacansies;
    }

    /**
     * @param string $vacansyName
     * @param int $idPage
     * @param int $idRegion
     *
     * @return array|null
     */
    private function get100vacansies(string $vacansyName, int $idPage, int $idRegion = self::ID_CITY_MOSCOW): ?array
    {
        // берем по 100 страниц.
        // page с 0  и макс до 19   // ограничение на вложенность до 2000 вакансий в сумме
        // надо ограничить по   area
        /*
         * запрос ниже возвращает массив с ключами
         * array(8) {
              [0]=>
              string(5) "items"
              [1]=>
              string(5) "found"
              [2]=>
              string(5) "pages"
              [3]=>
              string(8) "per_page"
              [4]=>
              string(4) "page"
              [5]=>
              string(8) "clusters"
              [6]=>
              string(9) "arguments"
              [7]=>
              string(13) "alternate_url"
            }
         *
         */
        $vacansies = $this->clientHh->vacancies->search([
            'text' => $vacansyName,
            'area' => $idRegion,
            'per_page' => 100,
            'page' => $idPage
        ]);

        return count($vacansies['items']) > 0 ? $vacansies['items'] : null;
    }
}