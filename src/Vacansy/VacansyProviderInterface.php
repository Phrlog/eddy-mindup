<?php


namespace App\Vacansy;


interface VacansyProviderInterface
{
    public function getVacancies($name, $idRegion);
}